# Project Title

## v0.1.0

<Short description about the project>

## Getting Started 🚀

These instructions will allow you to get a copy of the project running on your local machine for development and testing purposes.
See **Deployment** to know how to deploy the project.

### Pre-requirements 📋

[//]: # (This is an internal comment not shown in the README visually)
[//]: # (What things do you need to work with the project and how to install them)
 
* [Node JS](https://nodejs.org)
* [Yarn](https://yarnpkg.com)

### Installation 🔧

[//]: # (A series of step-by-step examples that tells you what to run to have a development environment running)

1. Clone repository:

        #!shell
        git clone <Repo URL>

1. Go to the project folder:

        #!shell
        cd <Project folder name>

1. Install dependencies:

        #!shell
        yarn install

1. Run the project:

        #!shell
        yarn start

    **NOTE:** Open `localhost:3000`

## Running the tests ⚙️

[//]: # (Explain how to run automated tests for this system)

### Running unit tests 🔩

<Explain what these tests verify and why>

```shell
yarn test
```

### Running linter 🖋

<Explain what these tests verify and why>

```shell
yarn lint
```

### Running Sonarqube

<Explain what these tests verify and why>

Before running this process it is necessary to configure the following environment variables in your system:

```shell
export SONAR_HOST_URL = <Sonarqube URL>
export SONAR_LOGIN = <Project Key>
```

Run analysis

```shell
yarn run sonarqube
```

## Deployment 📦

<Add additional notes on how to deploy>

### Build project 🔨

<Add aditional notes about building, if necessary>

```shell
yarn build
```

### Deploy project 🤖

<Add aditional notes about deployment, if necessary>

```shell
yarn deploy
```

## Built with 🛠️

[//]: # (Mention the development libraries and frameworks you used to create your project)


* [React JS](https://reactjs.org/)

## Extra Steps ⚡

<Add extra steps that you can run as part of building tasks>

## Wiki 📖

<If the project has a wiki add link here>

## Versioning 📌

For all available versions in the package, use [Cloudsmith](https://cloudsmith.io/).

## License 📄

```
This source code is the confidential, proprietary information of
Cafeto Software S.A.S., you may not disclose such Information,
and may only use it in accordance with the terms of the license
agreement you entered into with Cafeto Software S.A.S.

2020: Cafeto Software S.A.S.
All Rights Reserved.
```
