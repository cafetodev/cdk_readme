[//]: # (This is an internal comment not shown in the README visually)
[//]: # (Internal comments provide guidance to create this README file)
[//]: # (PLEASE DON'T remove internal comments)
# Project Title

## v0.1.0

[//]: # (Add a short description about this project)

## Getting Started 🕹️

These instructions will allow you to get a copy of the project running on your local machine for development and testing purposes.
See **Deployment** to know how to deploy the project.

### Pre-requirements 📋
[//]: # (What things do you need to work with the project and how to install them. i.e.: IDEs, tools, etc.)
 
* [Tool_1](https://cafeto.co/)
* [Tool_2](https://cafeto.co/)

### Installation 🔧
[//]: # (A series of step-by-step examples that tells you what to run to have a development environment running)

1. Clone repository:

        #!shell
        git clone <Repo URL>


1. Go to the project folder:

        #!shell
        cd <Project folder name>

1. Install dependencies and build the project:

        #!shell
        command

1. Run the project:

        #!shell
        command

**NOTE:** For local development, you must configure following environment variables in your system:


## Tests ⚙️
[//]: # (Explain how to run automated tests for this system)

### Execute Unit Tests 🔩
[//]: # (Explain what these tests verify and why)

        #!shell
        command


### Execute linter 🖋
[//]: # (Explain what these tests verify and why)

        #!shell
        command

### Execute Sonarqube 🛡️
[//]: # (Explain what these tests verify and why)

Before running this process it is necessary to configure the following environment variables in your system:

        #!shell
        export SONAR_HOST_URL = <Sonarqube URL>
        export SONAR_LOGIN = <Your sonarqube user token>

**NOTE:** To include the test coverage report in Sonarqube analysis you may require executing unit tests previously.

Run analysis

        #!shell
        command


## Deployment 🚀
[//]: # (Add additional notes on how to deploy)

### Build artifact 🔨
[//]: # (Add aditional notes about building, if necessary)

        #!shell
        command


### Publish artifact 📦
[//]: # (Add aditional notes about deployment, if necessary)

Before running this process it is necessary to configure the following environment variables in your system:

Java projects:

        #!shell
        export ARTIFACT_SERVER_USERNAME = <Artifact server user with permissions to publish>
        export ARTIFACT_SERVER_TOKEN = <Artifact server user token>

JavaScript projects:

        #!shell
        export CLOUDSMITH_API_KEY = <Artifact server api key with permissions to publish>

        #!shell
        command


### Deploy project 🤖
[//]: # (Add aditional notes about deployment, if necessary)

        #!shell
        command


## Built with 🛠️
[//]: # (Mention the development libraries and frameworks you used to create your project)

* [Framework_1](https://cafeto.co/)
* [Library_1](https://cafeto.co/)

## Extra Steps ⚡
[//]: # (Add extra steps that you can run as part of building tasks)

## Wiki 📖
[//]: # (If the project has a wiki add link here)

## Versioning 📌

Given a version number MAJOR.MINOR.PATCH, increment the:

- MAJOR version when you make incompatible API changes
- MINOR version when you add functionality in a backwards compatible manner
- PATCH version when you make backwards compatible bug fixes

You can read more about this project versioning scheme at https://semver.org/

## License 📄
[//]: # (Add the License declaration or its link here)

